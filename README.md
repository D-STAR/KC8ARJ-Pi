# KC8ARJ Raspberry Pi files

This repository contains files loaded on to the default compass
distro to implement the D-STAR repeater:

* etc
  * wpa_supplicant.conf - WiFi configuration
  * hosts - list of IP addresses
* etc/opendv
   * dstarrepeater_1 - repeater configuration
   * ircddbgateway - gateway configuration
* root
 * backitup - back up log files
 * cleanLogs - deleteold log files
 * getit - check to see if time to reboot
 * rungetit - run getit from the crontab and log
 * status - a simple script to show repeater status
* usr
* usr/bin
  * dstarrepeaterconfig - python script to configure repeater
  * ircddbgatewayconfig - python script to configure gateway
  * emacs - text editor
  * lynx - text browser for setting Karma password
* usr/sbin
  * dstarrepeaterd - repeater daemon
  * ircddbgatewayd - gateway daemon
  * timeserverd - timeserver daemon
  * miniupnpc - interface to gateway
  * iftop - display interface usage
* var
* var/spool/cron/crontabs
  * root - root's cron tab
  
----

The software is installed on the "Compass" distro from NW Digital Radio,
which has a number of application installed on top of the distro, so
getting the distro from them is important.

Unfortunately, there is a lot of unnecessary cruft which they didn't
uninstall. So there are word processors, games, etc. which are totally
useless on a repeater.

In addition to the NW Digital Radio stuff, the following were installed:

* dstarrepeaterd
* ircddbgatewayd
* timeserverd
* miniupnpc
* emacs
* iftop
* lynx

